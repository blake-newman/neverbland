require.register('injector', function(exports, require, module) {
  'use strict';
  var Injector;
  return module.exports = exports = Injector = (function() {
    // Constructor
    function Injector() {}
    
    // Check if can be injected
    Injector.check = function(attach) {
      return $('[data-attach="' + attach + '"]') ? true : false;
    };

    // Inject component only if it has a place to attach to
    Injector.component = function(name, options) {
      var Component;
      if (options == null) {
        options = {};
      }
      if (Injector.check(name)) {
        options.name = name;
        Component = new Tweak.Component(window, options);
        Component.init();
        Component.render();
      }
    };

    // Inject template onlu if it has an attachment point
    Injector.template = function(template, options) {
      var Component;
      if (options == null) {
        options = {};
      }
      if (Injector.check(template)) {
        options.name = template;

        if (options.view == null) {
          options.view = {};
        }

        if (options.view.template == null) {
          options.view.template = 'src/templates/' + template + '.hbs'
        }

        Component = new Tweak.Component(window, options);
        Component.init();
        Component.render();
      }
    };

    return Injector;

  })();
});