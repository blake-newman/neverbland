/*
  This is the lightbox module. When a lightbox image is open it will create a new lightbox
*/
require.register('lightbox', function(exports, require, module) {

  var Lightbox, lightbox;

 
  // Register the default lightbox config object
  window.require.register('lightbox/config', function(exports, require, module) {
    module.exports = {
      view: {
        template: 'src/templates/lightbox.hbs'
      }
    };
  });

  // Register the slider view
  window.require.register('lightbox/view', function(exports, require, module) {
    var LightboxView;
    return module.exports = exports = LightboxView = (function() {

      Tweak['extends'](LightboxView, Tweak.View);

      function LightboxView(obj) {
        Tweak.super(LightboxView, this, 'constructor', obj);
      }

      LightboxView.prototype.ready = function() {
        var that = this;
        TweenMax.delayedCall(0.05, function(){
          that.$el.addClass('open'); 
        });

        // Allows for 1 second animation before element is destroyed
        this.element('.close').on('click', function(){
          that.$el.addClass('close');
          TweenMax.delayedCall(1, function(){
            that.component.destroy();
          });
        });

      };

      return LightboxView;

    })();
  });

  
  // Main module   
  return module.exports = exports = Lightbox = (function() {
    function Lightbox() {}

    Lightbox.create = function(options) {
      var lightbox;
      options['extends'] = 'lightbox';
      lightbox = new Tweak.Component(window, options);
      lightbox.init();
      lightbox.render();
      return lightbox;
    };

    return Lightbox;

  })();
});
