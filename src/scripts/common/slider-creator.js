(function() {

  require.register('slider-creator', function (exports, require, module) {

    module.exports = exports = (function(){

      var injector, Slider, sliderDefault, sliderUnique, effectGenerator, effect, totalEffects, mapSlides, _slideMap;
      totalEffects = 4;


      injector = require('injector');


      // Check if slider is on page
      if(injector.check('slider')){
        Slider = new require('slider');  

        

        // Generate effect number
        effectGenerator = require('effect-generator');
        effect = effectGenerator.generate('slider', totalEffects);


        // The slide data could be generated from a server, but for this it will be static data
        // It looks a bit messy, but in a normal project you would just have a single config object
        // this just makes it simpler for me to show differnt effects / slider styles
        _slideMap = {
          0: {
            title: 'The Man Who Climbed The Wall',
            sub: 'CLIENT / AGENCY',
            image: 'images/slider-image.jpg'            
          }
        };

        _slideMap[1] = Tweak.combine({
          description: 'This is a description'
        }, _slideMap[0]);

        mapSlides = function(map){
          var i, result;
          result = [];
          for (i = 0; i < map.length; i++) {
            result.push(_slideMap[map[i]]);
          };
          return result;
        }

        // Default configuration for slider
        sliderDefault = {
          name: 'slider',
          extends: 'slider',
          model:{
            cycle:5
          },
          view: {
            attach: 'slider',
            method: 'before'            
          }
        };

        // Create unique slider properties
        // Each slider will alter its style by a few config options
        switch (effect) {
          case 1:
            sliderUnique = {
              model: {
                arrows:true,
                effect: 'inout',
                slides: mapSlides([0,0,0])
              }
            };
            break;
          case 2:
            sliderUnique = {
              model: {
                arrows: true,
                loop:false,
                slides: mapSlides([0,0,0,0])
              }
            }
            break;
          case 3:
            sliderUnique = {
              model: {
                arrows:true,
                effect: 'vertical',
                slides: mapSlides([0,1,0,1,0,1])
              }
            }
            break;
          case 4:
           sliderUnique = {
              model: {
                effect: 'outin',
                counter: false,
                arrows: true,
                slides: mapSlides([1,1,1,1,1,1])
              }
            }
            break;
        };
        // Generate slider with config
        Slider.create(Tweak.combine(sliderDefault, sliderUnique));

      };
    })();

  });

})();