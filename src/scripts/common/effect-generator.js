/*
  This module will generate a number repesenting an affect. It will store the current affect to local storage
  so when there is a refresh it will generate a different effect.
*/
(function() {
  "use strict";

  require.register('effect-generator', function (exports, require, module){
    
    var EffectGenerator, newEffect, hasLocalStorage, effectNum;
    
    module.exports = exports = EffectGenerator  = (function() {
      function EffectGenerator() {};

      EffectGenerator.generate = function(name, max){
        hasLocalStorage = $('html').hasClass('localstorage');
        // If local storage then then loop through slider in order else randomly generate affect number
        if (hasLocalStorage){
          effectNum = Number(window.localStorage.getItem(name+'-affect') || 1);
          newEffect = effectNum + 1;
          newEffect = newEffect > max ? 1 : newEffect;
          window.localStorage.setItem(name+'-affect', newEffect);
        } else {
          effectNum = Math.floor((Math.random() * max) + 1);
        }

        return effectNum;
      
      };

      return EffectGenerator;

    })();

  }); 

})();