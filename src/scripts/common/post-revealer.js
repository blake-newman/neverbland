/*
  A little revealer module to hide and show an element.
*/

require.register('post-revealer', function(exports, require, module) {
  'use strict';
  var PostRevealer;
  module.exports = exports = PostRevealer = (function() {
    var $postExpand;

    function PostRevealer() {}


    // Expand button to reveal more content.
    // Would have been easier just to use the jQuery method, but where is the fun in that.
    $postExpand = $('.post .article-expand');
    $postExpand.on('click', function() {
      var $extra, $this, _height0;

      // assign height 0 TweenMax options to _height0
      _height0 = {height: '0px'};
      $this = $(this);

      // Get relating extra to article
      $extra = $('.article-extra', $this.parent());
      if ($this.hasClass('showing')) {
        TweenMax.to($extra, 0.7, _height0);
      } else {
        // Initially set height of box to max height so tween max can calculate the height
        TweenMax.to($extra, 0, {height: 'auto'});        
        // Tween from 0px now Tweenmax has height of box
        TweenMax.from($extra, 0.7, _height0);
      }
      $this.toggleClass('showing');
    });

    return PostRevealer;

  })();
});