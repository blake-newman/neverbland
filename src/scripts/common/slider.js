require.register('slider', function(exports, require, module) {
  "use strict";

  var Slider;

  // Register the default slider config object  
  window.require.register('slider/config', function(exports, require, module) {
    module.exports = {
      model: {
        loop: true,
        cycle: false,
        cycleDir: 1,
        current: 0,
        arrows: false,
        counter: true,
        effect: 'horizontal',
        speed: 1,
        ease: ['Quart', 'easeInOut']
      },
      view: {
        template: 'src/templates/slider.hbs'
      }
    };
  });

  // Register the slider model
  window.require.register('slider/model', function(exports, require, module) {
    var SliderModel;
    return module.exports = exports = SliderModel = (function() {

      Tweak.extends(SliderModel, Tweak.Model);

      function SliderModel(obj) {
        Tweak.super(SliderModel, this, 'constructor', obj);
      }

      SliderModel.prototype.hasArrows = function() {
        return this.get('arrows') === true;
      };

      SliderModel.prototype.hasCounter = function() {
        return this.get('counter') === true;
      };

      SliderModel.prototype.isLast = function(num, total) {
        if (total == null) {
          total = this.get('slides').length - 1;
        }
        return num === total;
      };

      SliderModel.prototype.isFirst = function(num) {
        return num === 0;
      };

      SliderModel.prototype.validate = function(num) {
        var total;
        total = this.get('slides').length - 1;

        if (this.isFirst(num+1)) {
          num = total;
        } else if (this.isLast(num-1, total)) {
          num = 0;
        }

        return num;
      };

      SliderModel.prototype.setter_previous = function(previous) {
        return this.validate(previous);
      };

      SliderModel.prototype.setter_next = function(next) {
        return this.validate(next);
      };

      SliderModel.prototype.setter_current = function(current) {
        var cycleDir, oldPos;

        oldPos = this.get('current');
        current = this.validate(current);
        this.set('previous', current - 1);
        this.set('next', current + 1);

        cycleDir = current < oldPos ? -1 : 1;

        if(this.isLast(oldPos) && this.isFirst(current)){
          cycleDir = 1;
        } else if (this.isFirst(oldPos) && this.isLast(current)){
          cycleDir = -1;
        }

        if (this.get('loop') === false) {
          if (this.isLast(current)) {
            cycleDir = -1;
          } else if (this.isFirst(current)) {
            cycleDir = 1;
          }
        }
        
        this.set({
          cycleDir: cycleDir
        });

        return current;
      };

      return SliderModel;

    })();
  });

  /*
    Register the slider view
   */
  window.require.register('slider/view', function(exports, require, module) {
    var SliderView;
    return module.exports = exports = SliderView = (function() {
      var context;

      Tweak.extends(SliderView, Tweak.View);

      function SliderView(obj) {
        Tweak.super(SliderView, this, 'constructor', obj);
      }

      /*
        Called when the component is rendered
       */

      SliderView.prototype.ready = function() {
        var cycle, that;
        context = this;        
        cycle = this.model.get('cycle');

        // Get wrapper and slides
        this.$wrapper = $(this.element('.slides-wrapper > .slides')[0]);
        this.$slides = this.element('> figure', this.$wrapper);

        // Listen to model changes
        this.model.addEvent('changed:previous', this.setPrevious, this);
        this.model.addEvent('changed:current', this.setCurrent, this);
        this.model.addEvent('changed:next', this.setNext, this);
        this.model.addEvent('changed:current', this.setHeight, this, 1);        
        this.model.addEvent('changed:effect', this.setEffect, this);
        
        // Set arrows if config allows
        if (this.model.hasArrows()) {
          this.setArrows();
        }

        // Set counter if config allows
        if (this.model.hasCounter()) {
          this.setCounter();
        }

        // setup auto cycle
        if (cycle !== false) {
          this.model.addEvent('changed:current', function() {
            TweenMax.killDelayedCallsTo(this.model.set);
            this.setCycle(cycle);
          }, this);
        }

        // Set initial effect
        this.setEffect(this.model.get('effect'));

        // Set dir slider will move 
        this.dir = 1;

        // Set the current position
        this.model.set('current', this.model.get('current'));
        

        // Keep the height of slider to the height of the heighst element
        setInterval(function(){
          context.setHeight();
        }, 200);

      };

      SliderView.prototype.setEffect = function(effect) {
        this.$el.addClass(this.model.get('effect'));
      };

      SliderView.prototype.setCycle = function(cycle) {
        var num;
        context.dir = context.model.get('cycleDir');
        num = context.model.get('current') + context.dir;
        TweenMax.delayedCall(cycle, context.model.set, ['current', num], context.model);
      };

      SliderView.prototype.disableArrows = function(disable) {
        // Loop through arrows
        this.$arrows.each(function() {
          var $this, i, item, length;
          $this = $(this);
          $this.removeClass('disabled');
          
          // Loop through disable array and disable the arrows if it matches
          i = 0;
          length = disable.length;
          for (; i < length; i++) {
            item = disable[i];
            if ($this.hasClass(item)) {
              $this.addClass('disabled');
            }
          }
          
        });
      };

      SliderView.prototype.checkArrows = function(num) {
        if (this.model.isLast(num)) {
          this.disableArrows(['right']);
        } else if (this.model.isFirst(num)) {
          this.disableArrows(['left']);
        } else {
          this.disableArrows([]);
        }
      };

      SliderView.prototype.setArrows = function() {
        this.$arrows = this.element('> .arrow');
        if (this.model.get('loop') === false) {
          this.model.addEvent('changed:current', this.checkArrows, this);
        }

        
        // this.element() will find elements relative to this view         
        return this.$arrows.on('click', function() {
          var $this, isLeft, num;
          $this = $(this);

          // Return if already animating 
          if (context._animating || $this.hasClass('disabled')) return;
          // Set to animating
          context._animating = true;
          
          isLeft = $this.hasClass('left');
          context.dir = isLeft ? -1 : 1;
          num = context.model.get(isLeft ? 'previous' : 'next');
          context.model.set('current', num);

        });
      };

      SliderView.prototype.setCounter = function() {
        this.$counters = this.element('> .counter li');
        this.model.addEvent('changed:current', this.checkCounter, this);

        this.$counters.on('click', function(){
          // Return if already animating 
          if (context._animating || $this.hasClass('disabled')) return;
          // Set to animating
          context._animating = true;
          context.model.set('current', $(this).index())
        });
      };

      SliderView.prototype.checkCounter = function(num) {
        this.$counters.removeClass('active');
        $(this.$counters[num]).addClass('active');

      };

      SliderView.prototype.setHeight = function() {
        var max;
        max = 0;
        // loop through each slide and find highest
        this.$slides.each(function() {
          var height;
          height = $(this).height();
          if (height > max) {
            max = height;
          }
        });
        this.$wrapper.height(max);
      };

      SliderView.prototype.setSlide = function(type, pos) {
        var $item;
        $item = $(this.$slides[pos]);
        this.$slides.removeClass(type);
        $(this.$slides[pos]).addClass(type);
        this['$' + type] = $(this.$slides[pos]);
      };

      SliderView.prototype.setPrevious = function(pos) {
        return this.setSlide('previous', pos);
      };

      SliderView.prototype.setCurrent = function(pos) {
        this.setSlide('current', pos);
        return this.animate(pos);
      };

      SliderView.prototype.setNext = function(pos) {
        return this.setSlide('next', pos);
      };

      /*
        This is a little obscure method to do things but it allows for each slide to be uniquely transitioned.
        So items can have card flip animations ect. Kinda makes logic harder but is much more flexible. 
        I also belive that when navigating  by counter, that it should go straight to the slide as it is cleaner.
      */
      SliderView.prototype.animate = function() {        
        var _ease, ani, completeDelay, current, ease, effect, next, previous, scale, speed, type;

        previous = this.$previous
        next = this.$next
        current = this.$current
        
        // Set initial configuration
        effect = this.model.get('effect');
        speed = this.model.get('speed');
        completeDelay = 0;
        _ease = this.model.get('ease');
        ease = window[_ease[0]][_ease[1]];        
        ani = {
          previous: {
            to: { ease: ease },
            from: {}
          },
          next: {
            to: { ease: ease },
            from: {}
          },
          current: {
            from: {},
            to: {
              ease: ease,
              onComplete: function() {
                TweenMax.delayedCall(completeDelay, function() {
                  context._animating = false;
                });
              }
            }
          }
        };

        // Switch through effects
        switch (effect) {
          case 'horizontal':
          case 'vertical':
          case 'inout':
          case 'outin':
            type = effect === 'vertical' ? 'top' : 'left';
            // Set positions based on type and direction of slider
            
            // Works out if previous needs to move
            ani.previous.to[type] = '-100%';
            ani.previous.from[type] = this.dir === 1 ? '0%' : '-100%';

            // works out if next needs to move
            ani.next.to[type] = '100%';
            ani.next.from[type] = this.dir === -1 ? '0%' : '100%';
            
            // works out wher current needs to move from
            ani.current.to[type] = '0%';
            ani.current.from[type] = this.dir === 1 ? '100%' : '-100%';

            if (effect === 'inout' || effect === 'outin') {
              scale = effect === 'inout' ? 1.1 : 0.9;
              TweenMax.to(this.$wrapper, speed / 2, {
                scale: scale,
                ease: ease
              });
              TweenMax.to(this.$wrapper, speed / 2, {
                scale: 1,
                ease: ease,
                delay: speed / 2
              });
            }
            break;
          default:
            // If no animation is found then reset animation
            this._animating = false;
        }
        // If there is an effect then animate it
        if (effect) {
          TweenMax.fromTo(previous, speed, ani.previous.from, ani.previous.to);
          TweenMax.fromTo(next, speed, ani.next.from, ani.next.to);
          TweenMax.fromTo(current, speed, ani.current.from, ani.current.to);
        }
      };

      return SliderView;

    })();
  });

  // Main module use to create a slider and render straight away 
  module.exports = exports = Slider = (function() {
    function Slider() {}

    Slider.create = function(options) {
      var slider;
      options['extends'] = 'slider';
      slider = new Tweak.Component(window, options);
      slider.init();
      slider.render();
      return slider;
    };

    return Slider;

  })();
});