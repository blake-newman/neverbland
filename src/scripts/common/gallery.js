/*
  This is the gallery module. When a gallery image is open it will create a new lightbox
*/
require.register('gallery', function(exports, require, module) {

  "use strict"

  var Gallery, Lightbox;

  Lightbox = require('lightbox');
 
  // Register the default gallery config object
  window.require.register('gallery/config', function(exports, require, module) {
    module.exports = {
      view: {
        template: 'src/templates/gallery.hbs'
      }
    };
  });

  // Register the slider view
  window.require.register('gallery/view', function(exports, require, module) {
    var GalleryView;
    return module.exports = exports = GalleryView = (function() {
      function GalleryView(obj) {
        Tweak.super(GalleryView, this, 'constructor', obj)
      }


      Tweak['extends'](GalleryView, Tweak.View);
   

      GalleryView.prototype.ready = function() {
        var $items, that;

        $items = this.element('> div');
        that = this;

        $items.on('click', function(){
          Lightbox.create({
            name:'lightbox',
            model: {
              image: $(this).attr('data-larger'),
              alt: $('img', this).attr('alt'),
              description: $(this).attr('data-description')
            }
          });
        })
      };

      return GalleryView;

    })();
  });

  
  // Main module   
  return module.exports = exports = Gallery = (function() {
    function Gallery() {}

    Gallery.create = function(options) {
      var gallery;
      options['extends'] = 'gallery';
      gallery = new Tweak.Component(window, options);
      gallery.init();
      gallery.render();
      return gallery;
    };

    return Gallery;

  })();
});
