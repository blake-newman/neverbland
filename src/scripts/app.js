(function() {
  "use strict";

  var inject, revealer, Gallery;

  require('jst-tweaked');

  inject = require('injector');
 
  // Inject header template
  inject.template('header', {
    view: {
      method: 'before'
    }
  });

  // Inject footer template
  inject.template('footer', {
    view: {      
      method: 'after'
    }
  });

  // require post revealer
  require('post-revealer');

  // require slide generator
  require('slider-creator');

  // require gallery if the page has a gallery attachment
  if(inject.check('gallery')){
    var items, i, length, j, prefix, ext;
    i = 0;
    length = 6;
    items = [];
    prefix = 'images/gallery/';
    ext = '.jpg';

    for(; i < length; i++){
      j = i+1;
      items.push({
        alt:'Gallery image',
        image:prefix+j+ext,
        larger:prefix+j+'-large'+ext
      });
    }

    Gallery = require('gallery'); 
    Gallery.create({
      name:'gallery',
      model:{
        items:items
      }
    }); 
  }
  


})();
