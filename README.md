# Blake Newman - Challenge
I hope you enjoy my final result. I have included everything apart from the node_modules and .git folders as its fairly large (and email failed to send). You will have to reinstall the modules if you want to rebuilt the application. I may have gone over the top on a lot of the challenge, but its better to do more than less.

## Introduction
My first step was to use your neverbuild generator, thought it may get me a couple of bonus points ;). However, I have heavily adapted it to my preferred set-up. I typically like to run things in a CommonJS based environment, it offers many benefits as well as keeping code in a modular format. To achieve the CommonJS environment I use commonjs-require-definition, which is used by brunch. Its very lightweight and easy to understand, making it perfect for modular front-end code. I don't usually use Grunt as it is rather time consuming to set-up, and slow compared to Brunch (very slow). Also it provides much better live reloads; CSS get injected so you see live CSS changes, and full page reloads on JS changes. I'm sorry if I have butchered your original set-up.

I have decided the best way to show of my development skills is if I use use my MVC Framework. Tweak.js is driven towards the front-end, and really makes use of modular structures, components and much more. It is similar to Backbone.js in terms of simplicity but with variations that encourage certain coding practises. I hope for it so mature, and ive had great feedback from the people already using it.

I have used git as my version control, so you can see how I have progressed through the challenge. https://bitbucket.org/blake-newman/neverbland/commits/all for a visualisation on my progress.

## Structure
The source is in the src folder and it builds to the build folder. Use 'grunt' to recompile the code. 'grunt dist' will minify and combine the code for a production environment.

## HTML
I have separated the files into separate templates using handlebars. This is to make parts of the HTML easier to read, and at the same time making it reusable. I have made it as semantic as I feel needed. 

## JS
I have a huge history in CoffeeScipt and JS. You may notice my code is very modularly structured, that's probably the CoffeeScript side of me kicking in. I have only used JS for the project as it will make more sense to you that way as I don't know how much background you have in CoffeeScript.

## Issues/Improvements

### Responsive
The website is fully responsive, I have just noticed the slider content could have done with a slight adjustment for smaller screens. However that's only superficial, and easily adjusted. Unfortunately I have run out of time to correct it sorry.

### Slider
I have made the slider display different effects and configurations upon page reload. Configurations can include no looping, no arrows or no counter + more. The effects are unfortunately not as extravagant as I was hoping for but they are still slick and really smooth. The slider is completely reusable and extend-able which makes it great for embedding throughout projects and large scale projects. The slider uses JS/GSAP as it animation platform to give more control, and advanced animations. Furthermore everything can be updated on the fly, like the effect can be changed, speed ect. I havn't applied these features due to time constraints - I was going to add a router so you could change the slider on the go. I could also easily add a feature to inject slides at any point, but that was out of the scope for things I wanted to do.

### Gallery/Lightbox
The lightbox has a nice simple animation, with feel of Google's Material Design about it. The animation is done in the CSS to show you I know my CSS from my JS. The gallery items zoom in and rotate (Puppy dog eyes alert!) I also improved the overlay. 

### I USED SCSS
I have organised it as you may expect. I've not got much history in bourbon/neat, so there is probably a lot more mixins ect that I could have used to make the scss more refined and the overall compiled more cross browser friendly. 

### Extras
I have made the READ MORE button change to SHOW LESS when post is open. It also extends the whole post to provide more focus to the user, as it was easy-ish to miss. They also smoothly animate the content in and they don't refresh the page.

### What I didn't do
I didn't optimize the <head> for SEO, didn't feel that it was what you where looking for. But if you want clarification that I know how to optimize for SEO then look at my website source, it also has things like gzip.

Optimize the image sizes. They are quite heavy in size, I could also have whacked them in a spite-sheet. 

COMMENTS - oops. I really should have commented more. I have commented a few areas to help describe what is happening. I have kept everything super neat; so I am sure you will be able to follow the code. Most of it should be self explanatory. If you have any issues, don't hesitate to contact me and i'll do my best to explain.

I've just started looking more into TDD, I enjoy the process behind it. As I'm still rusty with the correct patterns to use, it is rather time consuming at the moment as i'm still seeking how to do it properly. So I would have applied it if I had time last minute but unfortunately I have no more time to show off. 


